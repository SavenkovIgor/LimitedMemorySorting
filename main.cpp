#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string>
#include <vector>
#include <algorithm>
#include <ctime>

/*
 *  Simple sorting in memory limit with merging
 *
 * Program generates file of random double numbers in standard format
 * and sorting it in memory limit 100 mb for ~12 minutes
 *
*/

using namespace std;

typedef vector<string> StringList;
typedef vector<int> IntList;

const int sortBufferSize = (100*1024*1024)/sizeof(double); // 12451840
const int generatedNumsCount = sortBufferSize*6;
const int halfBufferSize = sortBufferSize/2;              // 6225920

vector<double> mainBuffer;

StringList packArgumentsToStringList(int argc, char* argv[]);

// Checkers
bool isGenerateMode(StringList lst);
bool isSortMode(StringList lst);
bool isCheckMode(StringList lst);

// Workers
void generateFile(string fileName);
void copyFile(string src, string dst);
void sortFile(string fileSrc);
void checkFileSorted(string fileName);

FILE* openFileAtNumber(string fileName, int number, string openMode = "r+");
int getFileOffsetForNumber(int number);

void loadHalfBuffer(string fileName, int index, int bufferOffset = 0);
void saveHalfBuffer(string fileName, int index, int bufferOffset = 0);
IntList getFileOffsets();

void sortFullBuffer();
void sortHalfBuffer();

void printElapsedTime(clock_t begin);

// Main
int main(int argc, char* argv[])
{
    StringList argsList = packArgumentsToStringList(argc,argv);
    srand (time(NULL));

    if( isGenerateMode(argsList) ) {

        clock_t begin = clock();
        generateFile(argsList[2]);
        printf("File generated!");
        printElapsedTime(begin);

    } else if( isCheckMode(argsList)) {

        clock_t begin = clock();
        checkFileSorted(argsList[2]);
        printElapsedTime(begin);

    }else if( isSortMode(argsList)) {

        clock_t begin = clock();

        string unsortedFileName = argsList[2];
        string sortedFileName = argsList[3];

        mainBuffer.reserve(sortBufferSize);

        copyFile(unsortedFileName, sortedFileName);
        sortFile(sortedFileName);

        printElapsedTime(begin);
        printf("File sorted!");

    } else {

        printf("Parameters:\n");
        printf("1. Generating of random file: generate <fileName>\n");
        printf("2. Sorting of file: sort <unsorted fileName> <sorted fileName>\n");
        printf("3. Checking for sorted: check <fileName>\n");

    }

    return 0;
}

bool isSortMode(StringList lst) {
    // sort <fileSrc> <fileDst>
    if(lst.size() == 4) {
        if(lst[1] == "sort") {
            return true;
        }
    }
    return false;
}

bool isGenerateMode(StringList lst) {
    // generate <fileName>
    if(lst.size() == 3) {
        if(lst[1] == "generate") {
            return true;
        }
    }
    return false;
}

bool isCheckMode(StringList lst) {
    if(lst.size() == 3) {
        if(lst[1] == "check") {
            return true;
        }
    }
    return false;
}

void generateFile(string fileName) {

    FILE* pFile = fopen (fileName.c_str() , "w");

    for(int i = 0; i < generatedNumsCount; i++) {
        if(i%1000000 == 0) {
            printf("Generated %d %%\n", int((float(i)/generatedNumsCount)*100));
        }
        fprintf(pFile, "%E\n", double(rand()));
    }
    fclose(pFile);
}

void checkFileSorted(string fileName) {

    FILE* pFile = fopen (fileName.c_str() , "r");

    double previousValue = 0.0;
    double currentValue = 0.0;

    fscanf(pFile,"%lf\n" , &previousValue);

    for(int i = 0; i < generatedNumsCount - 1; i++) {
        if(i%1000000 == 0) {
            printf("Checked %d %%\n", int((float(i)/generatedNumsCount)*100));
        }
        fscanf(pFile,"%lf\n", &currentValue);
        if(previousValue > currentValue) {
            printf("File NOT sorted!\n");
            return;
        } else {
            previousValue = currentValue;
        }
    }
    fclose(pFile);
    printf("File Sorted!\n");
}

void loadHalfBuffer(string fileName, int index, int bufferOffset) {
    IntList offsets = getFileOffsets();
    if(index < 0 || index >= offsets.size())
        return;
    FILE* file = openFileAtNumber(fileName,offsets[index]);
    double value;
    int deleteLast = 0;
    for(int i = bufferOffset; i < bufferOffset + halfBufferSize; i++) {
        fscanf(file, "%lf\n", &value);
        mainBuffer[i] = value;
    }
    fclose(file);
}

void saveHalfBuffer(string fileName, int index, int bufferOffset) {
    IntList offsets = getFileOffsets();
    if(index < 0 || index >= offsets.size())
        return;
    FILE* file = openFileAtNumber(fileName,offsets[index]);
    for(int i = bufferOffset; i < bufferOffset + halfBufferSize; i++) {
        fprintf(file, "%E\n", mainBuffer[i]);
    }
    fclose(file);
}

void saveFullBuffer(string fileName, int sectionOne, int sectionTwo) {
    IntList offsets = getFileOffsets();

    if(sectionOne < 0 || sectionOne >= offsets.size())
        return;

    if(sectionTwo < 0 || sectionTwo >= offsets.size())
        return;

    int bufLeftIndex = 0;
    int bufRightIndex = halfBufferSize;

    FILE* leftFilePos = openFileAtNumber(fileName,offsets[sectionOne]);
    FILE* rightFilePos = openFileAtNumber(fileName,offsets[sectionTwo]);

    bool leftLesser = false;
    double saveValue = 0.0;

    for(int i = 0; i < sortBufferSize; i++) {

        leftLesser = mainBuffer[bufLeftIndex] < mainBuffer[bufRightIndex];

        if(bufLeftIndex == halfBufferSize) {// atEndLeft
            saveValue = mainBuffer[bufRightIndex];
            bufRightIndex++;
        } else if(bufRightIndex == sortBufferSize) {
            saveValue = mainBuffer[bufLeftIndex];
            bufLeftIndex++;
        } else {
            if(leftLesser) {
                saveValue = mainBuffer[bufLeftIndex];
                bufLeftIndex++;
            } else {
                saveValue = mainBuffer[bufRightIndex];
                bufRightIndex++;
            }
        }
        if(i < halfBufferSize) {
            fprintf(leftFilePos,"%E\n", saveValue);
        } else {
            fprintf(rightFilePos,"%E\n", saveValue);
        }
    }

    fclose(leftFilePos);
    fclose(rightFilePos);
}

IntList getFileOffsets() {
    IntList ret;
    ret.push_back(0);
    for(int i = 0; i < 1000; i++) {
        if(ret.back() + halfBufferSize < generatedNumsCount) {
            ret.push_back(ret.back() + halfBufferSize);
        } else {
            break;
        }
    }
    return ret;
}

void copyFile(string src, string dst) {
    printf("Copying file");
    FILE* srcFile = fopen(src.c_str(), "r");
    FILE* dstFile = fopen(dst.c_str(), "w");
    double value = 0.0;
    for(int i = 0; i < generatedNumsCount; i++) {
        if(i%1000000 == 0) {
            printf("Copyed %d %%\n", int((float(i)/generatedNumsCount)*100));
        }
        fscanf(srcFile, "%lf\n", &value);
        fprintf(dstFile, "%E\n", value);
    }
    fclose(srcFile);
    fclose(dstFile);
    printf("File copyed");
}

void sortFile(string fileSrc) {
    IntList offsets = getFileOffsets();
    printf("Prepare buffers\n");
    for(int i = 0; i < offsets.size(); i++) {
        printf("Offset: %d\n", offsets[i]);
        printf("Start loading %d buffer.\n", i);
        mainBuffer.resize(halfBufferSize);
        mainBuffer.clear();
        loadHalfBuffer(fileSrc, i);
        printf("Sorting.\n");
        sortHalfBuffer();
        printf("Saving.\n");
        saveHalfBuffer(fileSrc, i);
        printf("%d buffer sorted\n", i);
    }

    mainBuffer.clear();
    mainBuffer.resize(sortBufferSize);

    printf("offsets count: %d\n", offsets.size());
    for(int i = 0; i < offsets.size(); i++) {
        for(int j = i+1; j < offsets.size(); j++) {
            printf("i: %d j: %d\n", i, j);
            loadHalfBuffer(fileSrc, i);
            loadHalfBuffer(fileSrc, j, halfBufferSize);
            saveFullBuffer(fileSrc, i, j);
        }
        printf("i: %d\n", i);
    }
}

void sortFullBuffer() {
    sort(mainBuffer.begin(), mainBuffer.begin() + sortBufferSize);
}

void sortHalfBuffer() {
    sort(mainBuffer.begin(), mainBuffer.begin() + halfBufferSize);
}

FILE* openFileAtNumber(string fileName, int number, string openMode) {
    FILE* pFile = fopen(fileName.c_str(), openMode.c_str());
    fseek(pFile, getFileOffsetForNumber(number), SEEK_SET);
    return pFile;
}

int getFileOffsetForNumber(int number) {
    return number*13;
}

StringList packArgumentsToStringList(int argc, char* argv[]) {
    StringList ret;
    for(int i = 0; i < argc; i++) {
        ret.push_back(string(argv[i]));
    }
    return ret;
}

void printElapsedTime(clock_t begin) {
    clock_t end = clock();
    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    printf("Elapsed time: %f", elapsed_secs);
}
